<?php

require '../classes/Formulario.php';
require '../includes/config.php';
$data=Formulario::indicador1();
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'layout/defaultHead.php';?>
</head>
<body>
	<div id="wrapper">
        <!-- Navigation -->
        <?php include 'layout/navbar.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Videoconferencias Interrumpidas por Problemas Tecnicos ordenadas por Numero de Causa</h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table table-bordered" width="100%" id="tablaProcesos" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>N&uacute;mero de causa</th>
                                    <th>Fecha de videoconferencia</th>
                                    <th>Hora</th>
                                    <th>Solicitante</th>
                                    <th>Observaciones</th>
                                    <th>Estado Inicial</th>
                                    <th>Estado Final</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                               
                                foreach ($data as $info){
                                    echo '<tr>';
                                    echo '<td>'. $info["nro_causa"] . '</td>';
                                    echo '<td>'. date('d-m-Y',strtotime($info["fecha"])) . '</td>';
                                    echo '<td>'. $info["hora"] . '</td>';
                                    echo '<td>'. $info["solicitante"] . '</td>';
                                    echo '<td>'. $info["descripcion"] . '</td>';
                                    echo '<td>'. $info['estado_inicial'] . '</td>';
                                    echo '<td>'. "Interrumpida por Problemas Tecnicos" . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            </div>
        </div>
    </div>
    <?php include 'layout/defaultFooter.php'; ?>
</body>
</html>

