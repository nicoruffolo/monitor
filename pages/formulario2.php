<?php

require '../classes/Formulario.php';
require '../includes/config.php';
$data=Formulario::indicador2();

?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'layout/defaultHead.php';?>
</head>
<body>
	<div id="wrapper">
        <!-- Navigation -->
        <?php include 'layout/navbar.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">VideoConferencias del ultimo mes</h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table table-bordered" width="100%" id="tablaProcesos" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Fecha de la VideoConferencia</th>
                                    <th>Hora</th>
                                    <th>N&oacute;umero de causa</th>
                                    <th>Motivo de la Videoconferencia</th>
                                    <th>Solicitante</th>
                                    <th>Numero de Interno</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                               
                                foreach ($data as $info){
                                    echo '<tr>';
                                    echo '<td>'. $info["fecha"] . '</td>';
                                    echo '<td>'. $info["hora"] . '</td>';
                                    echo '<td>'. $info["nro_causa"] . '</td>';
                                    echo '<td>'. $info["motivo"] . '</td>';
                                    echo '<td>'. $info["solicitante"] . '</td>';
                                    echo '<td>'. $info["interno_id"] . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            </div>
        </div>
    </div>
    <?php include 'layout/defaultFooter.php'; ?>
</body>
</html>

