<?php

require '../classes/Formulario.php';
require '../includes/config.php';
$data=Formulario::indicador3();

?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'layout/defaultHead.php';?>
</head>
<body>
	<div id="wrapper">
        <!-- Navigation -->
        <?php include 'layout/navbar.php'; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Top 3 de los Internos con mas causas judiciales</h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table table-bordered" width="100%" id="tablaProcesos" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>N&uacute;mero de Interno</th>
                                    <th>Nombre del Interno</th>
                                    <th>Apellido del Interno</th>
                                    <th>N&uacute;mero de Unidad</th>
                                    <th>Cantidad de Causas</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php     
	                                foreach ($data as $info){
    	                                echo '<tr>';
        	                            echo '<td>'. $info["interno_id"] . '</td>';
            	                        echo '<td>'. $info["nombre"] .'</td>';
                                        echo '<td>'. $info["apellido"] . '</td>';
                                        echo '<td>'. $info["unidad_id"] . '</td>';
                                        echo '<td>'. $info["cantidad"] . '</td>';
                	                    echo '</tr>';
                    	            }
                                ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            </div>
        </div>
    </div>
    <?php include 'layout/defaultFooter.php'; ?>
</body>
</html>

